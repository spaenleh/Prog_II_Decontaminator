//
//  simulation.h
//  Prog_II_Decontaminator
//
//  Created by Basile & Nicolas on 21/03/2018.
//  Copyright © 2018 Basile & Nicolas. All rights reserved.
//	Modified on 19/04/2018.

#ifndef SIMULATION_H
#define SIMULATION_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>


#include "robot.h"
#include "particule.h"
#include "constantes.h"
#include "error.h"


/**
 * \brief	choisi un nombre aléatoire pour atribuer un but en cas de collision
 */
int simulation_random_particule(void);

/**
 * \brief	créé le tableau dynamique des buts des robots
 */
void simulation_creat_goal_tab(void);

/**
 * \brief	met à jour les buts des robots
 * \param	blocage
 */
void simulation_goal(bool blocage);

/**
 * \brief	renvoie le nombre de turn pour la simulation
 */
int simulation_get_turn(void);

/**
 * \brief	responsable d'un incrément de la simulation
 * \param	vtran_man	vrot_man
 */
bool simulation_step(double vtran_man, double vrot_man);

/**
 * \brief	ouvre le fichier de lecture
 * \param	nom_fichier
 */
bool simulation_lecture(const char * nom_fichier);

/**
 * \brief	déchiffre une ligne
 * \param	tab
 */
void simulation_decodage_ligne(char * tab);

/**
 * \brief	déchiffre le nombre de robots
 * \param	tab
 */
void simulation_decodage_nb_robot(char * tab);

/**
 * \brief	déchiffre les robots
 * \param	tab
 */
void simulation_decodage_robot(char * tab);

/**
 * \brief	recherche le fin de liste
 * \param	tab
 * \param	robot sert à indiquer si on est pour robot ou particule
 */
void simulation_decodage_fin_liste(char * tab, bool robot);

/**
 * \brief	déchiffre nb particule
 * \param	tab
 */
void simulation_decodage_nb_particules(char * tab);

/**
 * \brief	déchiffre une particule
 * \param	tab
 */
void simulation_decodage_particule(char * tab);

/**
 * \brief	détecte les lignes inutilles
 * \param	tab
 */
bool simulation_useless_line(char * tab);

/**
 * \brief	recherche FIN_LISTE
 * \param	tab
 */
bool simulation_fin_liste(char * tab);

/**
 * \brief	ferme les fichiers ouverts
 */
void simulation_close_exit(void);

/**
 * \brief	dessiner l'etat de la simulation
 */
void simulation_draw_state(void);

/**
 * \brief	libère la mémoire des structures
 */
void simulation_free_all(void);

/**
 * \brief	déterminer si on est mode Draw ou Error
 */
void simulation_set_mode(char * mode_entree);

/**
 * \brief	lorsqu'une erreur apparaît alors on déclanche
 * 			la mise hors tension de la lecture
 */
void simulation_error_detected(void);

/**
 * \brief	détecte les colosiona R-P
 */
void simulation_collision_particule_robot(int state);

/**
 * \brief	enregistre l'état courant de la simulation dans save.txt
 * \param	nom_fichier
 */
void simulation_enregistrement(const char * nom_fichier);

/**
 * \brief	enregistre le taux de décontamination dans out
 * \param	out 	checkbox
 */
void simulation_record(const char * out, bool checkbox);

#endif
