//
//  main.cpp
//  Prog_II_Decontaminator
//
//  Created by Basile & Nicolas on 21/03/2018.
//  Copyright © 2018 Basile & Nicolas. All rights reserved.
//	Modified on 21/05/2018.


#include   <GL/glu.h>
#include   <GL/glut.h>
#include   <GL/glui.h>


extern   "C"  {
#include "simulation.h"
}

namespace 
{
	int main_window;
	int width, height;
	GLfloat aspect_ratio;
	char opening[TEXTSIZE];
	char saving[TEXTSIZE];
	bool mode_manuel = false;
	bool sim_running = false;
	bool launch = false;
	const char * default_name = "save.txt";
	const char * default_out = "out.dat";
	char translation_default[] = "Translation: 0.000";
	char rotation_default[] = "Rotation: 0.000";
	char rate_default[] = "Rate: 0.000";
	char turn_default[] = "Turn: 0";

	GLUI *glui;

	GLUI_Panel *panel_open;
	GLUI_Panel *panel_save;
	GLUI_Panel *panel_simulation;
	GLUI_Panel *panel_recording;
	GLUI_Panel *panel_control_mode;
	GLUI_Panel *panel_robot;
	
	GLUI_EditText   *edittext1;
	GLUI_EditText   *edittext2;
	GLUI_StaticText *txt_rate;
	GLUI_StaticText *txt_turn;
	GLUI_StaticText *txt_tran;
	GLUI_StaticText *txt_rot;
	
	GLUI_Button *button_open;
	GLUI_Button *button_save;
	GLUI_Button *button_start;
	GLUI_Button *button_step;
	GLUI_Button *button_exit;
	
	GLUI_Checkbox *checkbox_record;
	
	GLUI_RadioGroup  *radio;
	GLUI_RadioButton *radio_man;
	GLUI_RadioButton *radio_auto;

	enum Constants {Z_MIN = -1, FLAGS, Z_MAX};
	enum Id {OPEN_B, SAVE_B, START, STEP, RECORD, CTRL_MODE, ROBOT_CTRL, EXIT};

	double vtrans = 0;
	double vrot = 0;
	int turn;
	double rate;
	bool no_error = true;
	bool robot_selected;
}	


void affichage(void)
{
	GLfloat g = -DMAX, d = DMAX, bas = -DMAX, haut = DMAX;
	
	glClear(GL_COLOR_BUFFER_BIT);
	
	glLoadIdentity();
	
	if(aspect_ratio <= ASPECT_RATIO)
		glOrtho(g, d, bas/aspect_ratio, haut/aspect_ratio, Z_MIN, Z_MAX);
	else
		glOrtho(g*aspect_ratio, d*aspect_ratio, bas, haut, Z_MIN, Z_MAX);

	//dessin de l'etat
	simulation_draw_state();
	glutSwapBuffers();
}


void maj_affichage_static_text(void)
{
	if(no_error)
	{
		char turn_text[TAILLE_CHAINE];
		char rate_text[TAILLE_CHAINE];

		turn = simulation_get_turn();
		rate = particule_taux();

		sprintf(turn_text, "Turn: %d", turn);
		sprintf(rate_text, "Rate: %.3lf", rate);
		txt_turn->set_text(turn_text);
		txt_rate->set_text(rate_text);
	}
}


void callback_start(void)
{
	if(!launch)
	{
		button_start->set_name("Stop");
		sim_running = true;
		launch = true;
	}
	else
	{
		if(sim_running)
		{
			button_start->set_name("Start");
			sim_running = false;
			simulation_record(default_out, false);
			checkbox_record->set_int_val(false);
		}
		else
		{
			button_start->set_name("Stop");
			sim_running = true;
		}
	}
}


void callback_ctrl_mode(void)
{
	if(radio->get_int_val())
		mode_manuel = true;
	else
	{
		mode_manuel = false;
		txt_tran->set_text(translation_default);
		txt_rot->set_text(rotation_default);
		vtrans = VTRAN_DEFAULT;
		vrot = VROT_DEFAULT;
		//CENTER paramètre sans importance
		robot_selection(CENTER, CENTER, mode_manuel);
	}
}


void callback_open(void)
{
	simulation_free_all();
	launch = false;
	sim_running = false;
	button_start->set_name("Start");
	txt_turn->set_text(turn_default);
	txt_rate->set_text(rate_default);
	simulation_record(default_out, false);
	checkbox_record->set_int_val(false);
	no_error = simulation_lecture(edittext1->get_text());
	
	if (no_error)
		simulation_goal(NO_BLOCAGE);
	
	affichage();
}


void control_cb(int control)
{
	switch(control)
	{
		case OPEN_B:
			callback_open();
			break;
			
		case SAVE_B:
			simulation_enregistrement(edittext2->get_text());
			break;
			
		case START:
			callback_start();
			break;
			
		case STEP:
			if(no_error)
				simulation_step(vtrans, vrot);
			maj_affichage_static_text();
			break;
			
		case RECORD:
			if(checkbox_record->get_int_val())
				simulation_record(default_out, true);
			else
				simulation_record(default_out, false);
			break;

		case CTRL_MODE:
			callback_ctrl_mode();
			break;
		
		case EXIT:
			simulation_free_all();
			simulation_close_exit();
			
		default: break;
	}
}


void mouse_cb(int button, int state, int x, int y)
{
	float glut_x = (DMAX + DMAX)*((float)x/width)-DMAX;
	float glut_y = DMAX - (DMAX + DMAX)*((float)y/height);

	if(button == GLUT_LEFT_BUTTON && state == GLUT_UP)
 	{	
 		if(mode_manuel)
 		{
 			if(robot_selection(glut_x, glut_y, mode_manuel))
 			{
 				simulation_goal(NO_BLOCAGE);
 				robot_selected = true;
 			}
 		}
 		else
 		{
			robot_selection(glut_x, glut_y, mode_manuel); 
 			robot_selected = false;
			
 			if(no_error)
 				simulation_goal(NO_BLOCAGE);
 		}
 	}
}


void special_cb(int key, int x, int y)
{
	char rotation[TAILLE_CHAINE];
	char translation[TAILLE_CHAINE];

	if(robot_selected)
	{	
		if(key ==  GLUT_KEY_UP)
		{
			if(vtrans < VTRAN_MAX)
				vtrans += VTRAN_INCREM;

			sprintf(translation, "Translation: %.3lf", vtrans);
			txt_tran->set_text(translation);
		}
		
		if(key == GLUT_KEY_DOWN)
		{	
			if(vtrans > -VTRAN_MAX)
				vtrans -= VTRAN_INCREM;

			sprintf(translation, "Translation: %.3lf",vtrans);
			txt_tran->set_text(translation);
		}

		if(key == GLUT_KEY_LEFT)
		{
			if(vrot < VROT_MAX)
				vrot += VROT_INCREM;

			sprintf(rotation, "Rotation: %.3lf", vrot);
			txt_rot->set_text(rotation);
		}	

		if(key == GLUT_KEY_RIGHT)
		{
			if(vrot > -VROT_MAX)
				vrot -= VROT_INCREM;

			sprintf(rotation, "Rotation: %.3lf",vrot);
			txt_rot->set_text(rotation);
		}	
	}
}


void idle(void)
{
	char rotation[TAILLE_CHAINE];
	
	if (no_error)
	{
		if (sim_running == true)
		{
			sim_running = simulation_step(vtrans, vrot);
			maj_affichage_static_text();

			if (sim_running == false)
			{
				button_start->set_name("Start");
				simulation_record(default_out, false);
				checkbox_record->set_int_val(false);
			}
		}
	}
	glutSetWindow(main_window);
	glutPostRedisplay();
}


void reshape(int w, int h)
{
	glViewport(0, 0, w, h);

	width = w;
	height = h;
	aspect_ratio = (GLfloat) w / (GLfloat) h;
	
	glutPostRedisplay();
}


void fenetre_glui(int argc, char * argv[])
{
	//widgets GLUI
	panel_open = glui->add_panel("Opening", GLUI_PANEL_EMBOSSED);
	edittext1 = glui->add_edittext_to_panel(panel_open, "File name:",
											GLUI_EDITTEXT_TEXT);
	if(argc < VALID_ARG)
		edittext1-> set_text(".txt");	
	else 	
		edittext1-> set_text(argv[NOM_FICHIER]);
		
	button_open = glui->add_button_to_panel(panel_open, "Open", OPEN_B,
											control_cb);
	panel_save = glui->add_panel("Saving", GLUI_PANEL_EMBOSSED);
	edittext2 = glui ->add_edittext_to_panel(panel_save, "File name:",
											 GLUI_EDITTEXT_TEXT);
	edittext2-> set_text(default_name);
	button_save = glui->add_button_to_panel(panel_save, "Save", SAVE_B, control_cb);
	glui->add_column(true);
	panel_simulation = glui->add_panel("Simulation", GLUI_PANEL_EMBOSSED);
	button_start = glui->add_button_to_panel(panel_simulation, "Start", START, 
											 control_cb);
	button_step = glui->add_button_to_panel(panel_simulation, "Step", STEP, 
											control_cb);
	panel_recording = glui->add_panel("Recording", GLUI_PANEL_EMBOSSED);
	checkbox_record = glui->add_checkbox_to_panel(panel_recording, "Record",
												  NULL, RECORD, control_cb);
	txt_rate = glui->add_statictext_to_panel(panel_recording, "Rate: 0.000");
	txt_turn = glui->add_statictext_to_panel(panel_recording, turn_default);
	glui->add_column(true);
	panel_control_mode = glui->add_panel("Control mode", GLUI_PANEL_EMBOSSED);
	radio = glui->add_radiogroup_to_panel(panel_control_mode, NULL, CTRL_MODE,
										  control_cb);
	radio_auto = glui->add_radiobutton_to_group(radio, "Automatic");
	radio_man = glui->add_radiobutton_to_group(radio, "Manual");
	panel_robot = glui->add_panel("Robot control", GLUI_PANEL_EMBOSSED);
	txt_tran = glui->add_statictext_to_panel(panel_robot, "Translation: 0.000");
	txt_rot = glui->add_statictext_to_panel(panel_robot, "Rotation: 0.000");
	button_exit = glui->add_button("Exit", EXIT, control_cb);
}


int main(int argc, char *argv[])
{
	if(argc < VALID_ARG)
		simulation_set_mode((char *) "Draw");
	else
	{
		simulation_set_mode(argv[MODE]);
		simulation_lecture((const char *) argv[NOM_FICHIER]);
	}

	glutInit(&argc, argv);
	glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );
	glutInitWindowPosition( WINDOWPOSITION, WINDOWPOSITION);
	glutInitWindowSize( WINDOWSIZE, WINDOWSIZE );
	main_window = glutCreateWindow("Decontaminators - Display" );
	glui = GLUI_Master.create_glui("Decontaminators - Control", FLAGS, 
								   WINDOWPOSITION+WINDOWSIZE, WINDOWPOSITION);
	graphic_set_background_color();
	GLUI_Master.set_glutIdleFunc(idle);
	glutDisplayFunc(affichage);
	glutReshapeFunc(reshape);
	GLUI_Master.set_glutMouseFunc(mouse_cb);
	GLUI_Master.set_glutSpecialFunc(special_cb);

	fenetre_glui(argc, argv);
	
	glui->set_main_gfx_window(main_window);
	//Start Glut Main Loop
	glutMainLoop();
	return EXIT_SUCCESS;
}
