//
//  robot.h
//  Prog_II_Decontaminator
//
//  Created by Basile & Nicolas on 17/05/2018.
//  Copyright © 2018 Basile. All rights reserved.
//	Modified on 21/05/2018.

#ifndef ROBOT_H
#define ROBOT_H

#include <stdio.h>
#include <stdlib.h>
#include "utilitaire.h"
#include "particule.h"
#include "constantes.h"
#include "error.h"


/**
 *	\brief	retourne un booléen vrai si robot d'indice i selectionné
 *	\param	i 
 */
bool robot_manual_control(int i);

/**
 *	\brief	calcule position future 
 *	\param	indice	vtran	vrot
 */
void robot_adapt_collision(int indice, double *vtran, double *vrot);

/**
 *	\brief	supprime particule si robot aligné
 *	\param	indice	vtran	but
 */
bool robot_suppression_part(int indice, double vtran, C2D but);

/**
 *	\brief	fais bouger le robot d'indice
 *	\param	indice	vtran	vrot
 */
void robot_mouvement(int indice, double *vtran, double *vrot);

/**
 *	\brief	se charge des collisions robot-robot
 *	\param	indice	objet	vtran	vrot
 */
void robot_collision_calcul(int indice, C2D objet, double *vtran, double *vrot);

/**
 *	\brief	recupere le C2D du robot
 *	\param	indice
 */
C2D robot_get_c2d(int indice);

/**
 *	\brief	trouve si il y a des collisions entre les robots
 *	\param	indice	vtran	vrot
 */
int robot_find_collision(int indice, double vtran, double vrot);

/**
 *	\brief	applique les vitesses desirées pour le robot indice
 *	\param	indice	but	 vtran	vrot
 */
void robot_evaluate_move(int indice, C2D but, double * vtran, double * vrot);

/**
 *	\brief	récupere l'angle du robot indice
 *	\param	indice
 */
double robot_get_angle(int indice);

/**
 *	\brief	alloue de la mémoire au tableau de robots
 *	\param	nb_robot
 */
bool robot_init_structure(int total_robot);

/**
 *	\brief	enregistre le robot i dans le tableau tab_robot
 *	\param	i	x	y	a
 */
bool robot_add_robot(int i, double x, double y, double a);

/**
 *	\brief	dessine les robots
 */
void robot_draw_state(void);

/**
 *	\brief	libère la structure de robots
 */
void robot_free_all(void);

/**
 *	\brief	détecte les collisions R-R
 */
bool robot_detection_collision(void);

/**
 *	\brief	donne les infos du robot demandé
 *	\param	indice
 */
C2D robot_current_c2d(int indice);

/**
 *	\brief	enregistre les infos des robots dans le fichier save
 *	\param	save
 */
void robot_enregistrement(FILE * save);

/**
 * \brief	determine le robot selectionné avec la souris
 * \param	x	y	mode
 */
bool robot_selection(double x, double y, bool mode);


#endif
