# Compilateur a utiliser :
##########################

CC     = gcc

# Options de compilation : (-g = pour deverminer)
#################################################

CFLAGS = -g


# Librairies a utiliser :
# LIBS: toutes les librairies qu'il faut intégrer
#########################

LIBS   = -lstdc++ -lglut -lGL -lGLU -lm -Llib -lglui -L/usr/X11R6/lib -lX11 \
	-lXext -lXmu -lXi

# Liste de fichiers source (.c) a compiler :
############################################

CFILES = main.cpp error.c particule.c robot.c simulation.c graphic.c utilitaire.c

# Liste de modules objets (generee a partir de CFILES) :
# Les fichiers objets (.o) ont le même nom que les fichiers source (.c)
# Seulement le suffixe change.
########################################################

OFILES = $(CFILES:.c=.o)

# Nom du fichier executable :
#############################

CIBLE  = projet.x

# edition de liens (rassembler tous les objets -> executable)
#############################################################

$(CIBLE): $(OFILES) 
	$(CC) $(OFILES) -o $(CIBLE) ${LIBS}

# Definitions de cibles particulieres :
#
# "make depend" : genere la liste des dependances
# "make clean"  : efface les fichiers .o et .x
#################################################

depend:
	@echo " *** MISE A JOUR DES DEPENDANCES ***"
	@(sed '/^# DO NOT DELETE THIS LINE/q' Makefile && \
	  $(CC) -MM $(CFLAGS) $(CFILES) | \
	  egrep -v "/usr/include" \
	 ) >Makefile.new
	@mv Makefile.new Makefile

clean:
	@echo " *** EFFACE MODULES OBJET ET EXECUTABLE ***"
	@/bin/rm -f *.o projet.x *.h.gch

#
# -- Regles de dependances generees par "make depend"
#####################################################
# DO NOT DELETE THIS LINE
main.o: main.cpp simulation.h robot.h utilitaire.h tolerance.h graphic.h \
 constantes.h error.h particule.h
error.o: error.c error.h constantes.h tolerance.h
particule.o: particule.c particule.h utilitaire.h tolerance.h graphic.h \
 simulation.h robot.h constantes.h error.h
robot.o: robot.c robot.h utilitaire.h tolerance.h graphic.h constantes.h \
 error.h
simulation.o: simulation.c simulation.h robot.h utilitaire.h tolerance.h \
 graphic.h constantes.h error.h particule.h
graphic.o: graphic.c graphic.h
utilitaire.o: utilitaire.c utilitaire.h tolerance.h graphic.h
