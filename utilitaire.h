#ifndef UTILITAIRE_H
#define UTILITAIRE_H

#include <stdbool.h>
#include "tolerance.h"
#include "graphic.h"

//
// Types concrets exportés par le module utilitaire
//

// type et structure permettant de représenter un point ou un vecteur 2D
typedef struct S2d S2D;
struct S2d
{
	double x;
	double y;
};

// type et structure représentant un cercle dans le plan 2D
typedef struct C2d C2D;
struct C2d
{
	S2D centre;
	double rayon;
};

// ensemble des fonctions exportées 
/**
 * \brief	renvoie la distance entre les points a et b
 * \param	a b
 */
double 	util_distance(S2D a, S2D b);

/**
 * \brief	renvoie l'angle que fait le bipoint ab avec l'axe x du monde
 *			angle en radians et dans l'intervalle ]-pi, +pi]
 * \param	a b
 */
double 	util_angle(S2D a, S2D b);

/**
 * \brief	modifie si nécessaire l'angle pointé par p_angle
 *			pour qu'il soit compris dans l'intervalle ]-pi, +pi]
 * \param	p_angle	angle à tester
 */
void 	util_range_angle(double * p_angle);

/**
 * \brief	renvoie VRAI si le point est en dehors du domaine [-max, max]
 * \param	a	max
 */
bool 	util_point_dehors(S2D a, double max);

/**
 * \brief	renvoie VRAI si l'angle alpha est en dehors de l'intervalle [-pi, pi]
 * \param	alpha	angle à tester
 */
bool 	util_alpha_dehors(double alpha);

/**
 * \brief	renvoie VRAI si le point a est dans le cercle c
 *			si la distance de a au centre de c  < rayon - EPSIL_ZERO
 * \param	a c
 */
bool 	util_point_dans_cercle(S2D a, C2D c);

/**
 * \brief	renvoie VRAI en cas de collision des cercles a et b selon l'Equ. 4
 *			p_dist est la distance entre les centres de a et b
 * \param	a	b	p_dist
 */
bool 	util_collision_cercle(C2D a, C2D b, double * p_dist);

/**
 * \brief	renvoie la position obtenue après déplacement du point p d'une
 *			distance dist dans la direction définie par l'angle alpha
 * \param	p	alpha	dist
 */
S2D 	util_deplacement(S2D p, double alpha, double dist);

/**
 *	\brief	renvoie VRAI si la distance de a à b > EPSIL_ZERO
 *			renvoie FAUX sinon
 *			si VRAI p_ecart_angle récupère l'écart angulaire entre ab et alpha
 *			l'écart angulaire doit être dans l'intervalle [-pi, pi]
 *	\param	a	alpha	b	p_ecart_angle
 */
bool 	util_ecart_angle(S2D a, double alpha, S2D b, double *p_ecart_angle);

/**
 *	\brief	renvoie VRAI si alpha est aligné avec le vecteur ab et si
 *			la valeur absolue de cet ecart est < EPSIL_ALIGNEMENT
 *			renvoie FAUX pour tous les autres cas
 *	\param	a	alpha	b
 */
bool 	util_alignement(S2D a, double alpha, S2D b);

/**
 *	\brief	calcule la nouvelle distance à parcourir en cas de collision
 *	\param	la	lb	lc	lb_new	p_la_new
 */
// renvoie VRAI si on peut calculer la nouvelle longueur du coté a lorsqu'on change
// la longueur du coté b, la longueur du coté c restant constante. Les longueurs des 
// cotés a,b,c sont notées la, lb, lc. La nouvelle longueur du coté b est lb_new. 
// le paramètre de sortie p_la_new doit être défini. Renvoie VRAI si:
// les 3 longueurs la, lb et lc > EPSIL_ZERO et lb_new se trouve dans l'intervalle
// autorisé [lb, lc]. Le calcul de la_new est donné par l'Equ.5 qui résoud le cas 
// particulier de la Fig 5c avec:
//     la     = delta_d, lb = D, lc = L, lb_new = r1+r2
//     la_new = delta_d'
bool 	util_inner_triangle(double la, double lb, double lc, double lb_new,
						    double * p_la_new);

/**
 *	\brief	dessine un robot
 *	\param 	robot 	angle 	selected
 */
void utilitaire_draw_robot(C2D robot, double angle, bool selected);

/**
 *	\brief	dessine une particule
 *	\param	particule
 */
void utilitaire_draw_particule(C2D particule);

/**
 *	\brief	dessine le cadre
 */
void util_dessiner_cadre(int length);

#endif
