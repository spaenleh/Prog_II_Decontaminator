//
//  robot.c
//  Prog_II_Decontaminator
//
//  Created by Basile & Nicolas on 17/05/2018.
//  Copyright © 2018 Basile & Nicolas. All rights reserved.
//	Modified on 19/04/2018.

#include "robot.h"

typedef struct Robot ROBOT;
struct Robot
{
	C2D robot;
	C2D but;
	double angle;
	bool selected;
};


//variables globales
static ROBOT * tab_robot = NULL;
static int nb_robots = 0;


bool robot_manual_control(int i)
{
	return tab_robot[i].selected;
}


double robot_get_angle(int indice)
{
	return tab_robot[indice].angle;
}


void robot_adapt_collision(int indice, double *vtran, double *vrot)
{
	C2D futur;
	C2D robot_select;
	int j;
	double dist;
	double angle_robot = tab_robot[indice].angle;
	double futur_angle = angle_robot + *vrot*DELTA_T;
	futur.centre = util_deplacement(tab_robot[indice].robot.centre,
									futur_angle, *vtran*DELTA_T);
	futur.rayon = R_ROBOT;

	for (j = 0; j < nb_robots; j++)
	{
		robot_select = tab_robot[j].robot;

		if ((indice != j) && util_collision_cercle(futur, robot_select, &dist))
			robot_collision_calcul(indice, robot_select, vtran, vrot);
	}
}


void robot_evaluate_move(int indice, C2D but, double * vtran, double * vrot)
{
	C2D forme = tab_robot[indice].robot;
	ROBOT * p_robot = tab_robot+indice;

	double angle_particule = util_angle(forme.centre, but.centre);
	double angle_dest = angle_particule - p_robot->angle;
	util_range_angle(&angle_dest);

	if(!tab_robot[indice].selected)
		*vrot = angle_dest / DELTA_T;

	*vtran = (util_distance(forme.centre, but.centre)-but.rayon-R_ROBOT)/DELTA_T;
	
	if(*vtran > VTRAN_MAX)
		*vtran = VTRAN_MAX;

	if(fabs(*vrot) > VROT_MAX)
	{
		if(*vrot > VITESSE_NULLE)
			*vrot = VROT_MAX;
		if(*vrot < VITESSE_NULLE)
			*vrot = -VROT_MAX;
	}

	if (!(angle_dest < M_PI*0.5 && angle_dest > -M_PI*0.5))
		*vtran = VITESSE_NULLE;
}


void robot_collision_calcul(int indice, C2D objet, double *vtran, double *vrot)
{
	double la = *vtran*DELTA_T;
	C2D forme = tab_robot[indice].robot;
	double new_angle = tab_robot[indice].angle + *vrot * DELTA_T;
	S2D nouvelle_pos = util_deplacement(forme.centre, new_angle, la);
	double lb = util_distance(nouvelle_pos, objet.centre);
	double lc = util_distance(forme.centre, objet.centre);
	double dist, lb_new = forme.rayon + objet.rayon;
	
	if(util_inner_triangle(la, lb, lc, lb_new, &dist))
		*vtran = dist/DELTA_T;
	else
		*vtran = VITESSE_NULLE;
}

void robot_mouvement(int indice, double *vtran, double *vrot)
{
	ROBOT *bot = tab_robot+indice;
	bot->angle += *vrot * DELTA_T;
	util_range_angle(&(bot->angle));
	bot->robot.centre = util_deplacement(bot->robot.centre, bot->angle,
										 *vtran*DELTA_T);
}


bool robot_suppression_part(int indice, double vtran, C2D but)
{
	double new_dist = util_distance(tab_robot[indice].robot.centre, but.centre) 
									- R_ROBOT - but.rayon;
	
	if (new_dist <= EPSIL_ZERO)
	{
		if (util_alignement(tab_robot[indice].robot.centre, tab_robot[indice].angle,
							but.centre))
		{
			particule_decontamination(particule_position_location(but.centre));
			particule_suppression_particule(particule_position_location(but.centre));
			return true;
		}
	}
	return false;
}


int robot_find_collision(int indice, double vtran, double vrot)
{
	int i;
	for (i = 0; i < nb_robots; i++)
	{
		double distance;
		if (i!=indice && util_collision_cercle(tab_robot[i].robot,
			tab_robot[indice].robot, &distance))
		{
			return i;
		}
	}
	return NO_ONE;
}


C2D robot_get_c2d(int indice)
{
	return tab_robot[indice].robot;
}


bool robot_init_structure(int total_robot)
{
	nb_robots = total_robot;
	if(total_robot)
	{
		if(!(tab_robot = (ROBOT*) calloc(total_robot, sizeof(ROBOT))))
		{
			printf("Allocation dynamique échouée dans la fonction %s.", __func__);
			return false;
		}
	}
	return true;
}


bool robot_add_robot(int i, double x, double y, double a)
{
	(tab_robot+i)->robot.centre.x = x;
	(tab_robot+i)->robot.centre.y = y;
	(tab_robot+i)->robot.rayon = R_ROBOT;
	(tab_robot+i)->angle = a;
	(tab_robot+i)->selected = false;
	if (util_alpha_dehors(a))
	{
		error_invalid_robot_angle(a);
		return false;
	}
	return true;
}


void robot_draw_state(void)
{
	int i;
	for (i=0; i<nb_robots; i++)
	{
		utilitaire_draw_robot(tab_robot[i].robot, tab_robot[i].angle, 
							  tab_robot[i].selected);
	}
}


void robot_free_all(void)
{
	if (tab_robot)
	{
		free(tab_robot);
		tab_robot = NULL;
	}
	nb_robots = NOBODY;
}


//return true si collision
bool robot_detection_collision(void)
{
	double distance;
	int i, j;
	for (i = 0; i < nb_robots-1; i++)
	{
		for (j = i+1; j<nb_robots; j++)
		{
			if(util_collision_cercle(tab_robot[i].robot, tab_robot[j].robot,
				&distance))
			{
				error_collision(ROBOT_ROBOT, i+1, j+1);
				return true;
			}
		}
	}
	return false;
}


bool robot_selection(double x, double y, bool mode)
{
	int i, j;
	S2D point;
	point.x = x;
	point.y = y;
	bool resultat = false;

	for(i = 0; i < nb_robots; i++)
	{
		if(mode && util_point_dans_cercle(point, tab_robot[i].robot))
		{
			tab_robot[i].selected = true;
			resultat = true;
		}
		else
			tab_robot[i].selected = false;		
	}
	return resultat;
}


C2D robot_current_c2d(int indice)
{
	return tab_robot[indice].robot;
}


void robot_enregistrement(FILE * save)
{
	int i;
	fprintf(save, "%d\n", nb_robots);

	for(i = 0; i < nb_robots; i++)
		fprintf(save, "%g %g %g\n", tab_robot[i].robot.centre.x,
				tab_robot[i].robot.centre.y, tab_robot[i].angle);
	
	if(nb_robots)
		fprintf(save, "FIN_LISTE\n");
}


