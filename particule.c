//
//  particule.c
//  Prog_II_Decontaminator
//
//  Created by Basile & Nicolas on 21/03/2018.
//  Copyright © 2018 Basile & Nicolas All rights reserved.
//	Modified on 21/05/2018.

#include "particule.h"
#include <assert.h>


struct Particule 
{
	C2D particule;
	double energie;
	PARTICULE * suivant;
	PARTICULE * precedent;
};

enum Nouvelles_particules{FIRST, SECOND, THIRD, FOURTH, MAX};

// créer un pointeur de structure pour la tête
static PARTICULE * particule_tete = NULL;
static int nb_particule;
static PARTICULE * collision_current = NULL;
static PARTICULE *p_fin_liste = NULL;
static double si = 0, sd = 0;


C2D particule_get_c2d_adr(PARTICULE* part)
{
	return part->particule;
}


PARTICULE * particule_get_adr(int part)
{
	PARTICULE * temp = particule_tete;
	int i;

	if(part != NOBODY)
	{
		for(i = 0; (i < part) && temp; i++)
			temp = temp->suivant;
	}
	return temp;
}


PARTICULE * particule_adapt_collision(C2D futur, double *vtran, double *vrot,
									  int indice)
{
	int part = 0;
	int j;
	double dist;
	PARTICULE * selected = NULL;
	PARTICULE * particule;
	C2D particule_selected;

	for (j = 0, particule = particule_tete; j < nb_particule && particule; j++, 
		 particule = particule->suivant)
	{
		if (util_collision_cercle(futur, particule->particule, &dist))
		{
			robot_evaluate_move(indice, particule->particule, vtran, vrot);
			selected = particule;
		}
	}
	return selected;
}


void particule_init_taux(void)
{
	si = 0;
	sd = 0;
}


C2D particule_get(int part)
{
	PARTICULE * temp = particule_tete;
	int i;

	if(part != NOBODY)
	{
		for(i=0; (i < part) && temp; i++)
			temp = temp->suivant;
	}
	return temp->particule;
}


PARTICULE * particule_find_closest(C2D robot)
{
	PARTICULE * temp = p_fin_liste;
	PARTICULE * selected = p_fin_liste;
	double distance_min, distance_current;
	int i;

	for(i = 0; i < nb_particule; i++)
	{
		distance_current = util_distance(robot.centre, temp->particule.centre);
		
		if(!i || distance_current <= distance_min)
		{
			distance_min = distance_current;
			selected = temp;
		}
		temp = temp->precedent;
	}
	return selected;
}


void particule_tri_insertion(void)
{
	if(!particule_tete->suivant)
	{
		p_fin_liste = particule_tete;
		return;
	}
	else
	{
		PARTICULE * sorted=NULL;
		PARTICULE * temp;
		PARTICULE * selected;
		PARTICULE * new = NULL;
		int i;
		for(i = nb_particule; i > 0; i--)
		{
			selected = particule_tete;
			for (temp = particule_tete->suivant; temp != NULL; temp=temp->suivant)
				if(temp->particule.rayon >= selected->particule.rayon)
					selected = temp;
			if(selected == particule_tete)
				particule_tete=selected->suivant;

			if(selected->precedent)
				(selected->precedent)->suivant = selected->suivant;

			if(selected->suivant)
				(selected->suivant)->precedent = selected->precedent;

			if(new == NULL)
			{
				new = selected;
				selected->precedent = NULL;
				selected->suivant = NULL;
			}
			else
			{
				sorted->suivant = selected;
				selected->precedent = sorted;
				selected->suivant = NULL;
			}
			sorted = selected;
		}
		p_fin_liste = sorted;
		particule_tete = new;
	}
}


void particule_set_nb_particule(int nb_objects)
{
	nb_particule = nb_objects;
}


bool particule_add_particule(int i, double e, double r, double x, double y)
{
	PARTICULE * new_particule = NULL;
	
	if (!(new_particule = (PARTICULE*) malloc(sizeof(PARTICULE))))
	{
		printf("Allocation dynamique échouée dans : %s\n",__func__);
		return false;
	}

	new_particule->precedent = NULL;
	if(particule_tete)
		particule_tete->precedent = new_particule;
	
	new_particule->suivant = particule_tete;
	particule_tete = new_particule;

	new_particule->particule.centre.x = x;
	new_particule->particule.centre.y = y;
	new_particule->particule.rayon = r;
	new_particule->energie= e;
	si += e;
	
	if(util_point_dehors(new_particule->particule.centre, DMAX)
	   || e < E_PARTICULE_MIN || e > E_PARTICULE_MAX || r < R_PARTICULE_MIN
	   || r > R_PARTICULE_MAX )
	{
		error_invalid_particule_value(e, r, x, y);
		return false;
	}
	return true;
}


S2D particule_get_s2d(int indice)
{
	int i;
	PARTICULE *temp = particule_tete;
	if(indice >= nb_particule)
		return particule_tete->particule.centre;

	for(i = 0; i < indice, temp->suivant; i++)
		temp = temp->suivant;
	return temp->particule.centre;
}


int particule_get_nb_particules(void)
{
	return nb_particule;
}


PARTICULE * particule_position_location(S2D position)
{
	PARTICULE *search = particule_tete;

	while(search)
	{
		if (util_point_dans_cercle(position, search->particule))
			return search;
		search = search->suivant;
	}
	return NULL;
}


PARTICULE * particule_rayon_location(double nv_rayon)
{
	PARTICULE *search = particule_tete;
	PARTICULE *select = NULL;

	while(search && search->particule.rayon >= nv_rayon)
	{
		select = search;
		search = search->suivant;
	}
	return select;
}


void particule_new_generation(PARTICULE *temp, double new_rayon, double new_energie)
{
	PARTICULE *new_part[MAX], *insertion = NULL, *stockage = NULL;
	double center_x = temp->particule.centre.x;
	double center_y = temp->particule.centre.y;
	int i;
	insertion = particule_rayon_location(new_rayon);
	for(i = 0; i < MAX; i++)
	{
		if (!(new_part[i] = (PARTICULE*) malloc(sizeof(PARTICULE))))
			printf("Allocation dynamique échouée dans : %s\n",__func__);
		
		new_part[i]->energie = new_energie;
		new_part[i]->particule.rayon = new_rayon;
		new_part[i]->precedent = NULL;
		if (i>FIRST)
		{
			new_part[i-1]->suivant = new_part[i];
			new_part[i]->precedent = new_part[i-1];
		}
		new_part[i]->suivant = NULL;
		switch(i)
		{
			case FIRST: 
			new_part[i]->particule.centre.x = center_x + new_rayon;
			new_part[i]->particule.centre.y = center_y + new_rayon;
			break;

			case SECOND:
			new_part[i]->particule.centre.x = center_x - new_rayon;
			new_part[i]->particule.centre.y = center_y - new_rayon;
			break;

			case THIRD:
			new_part[i]->particule.centre.x = center_x - new_rayon;
			new_part[i]->particule.centre.y = center_y + new_rayon;
			break;

			case FOURTH:
			new_part[i]->particule.centre.x = center_x + new_rayon;
			new_part[i]->particule.centre.y = center_y - new_rayon;
			break;
		}
	}
	if(insertion->suivant)
	{
		stockage = insertion->suivant;
		(insertion->suivant)->precedent = new_part[FOURTH];
		new_part[FOURTH]->suivant= stockage;
	}	
	new_part[FIRST]->precedent = insertion;;
	insertion->suivant = new_part[FIRST];
	nb_particule += NB_PART_DECOMPOSE;
}


bool particule_decomposition(void)
{
	PARTICULE *temp = particule_tete;
	PARTICULE *ok = NULL;
	PARTICULE *delete = NULL;

	PARTICULE *insertion = NULL;
	double new_rayon;
	double nouvelle_energie;
	bool decompose = false;

	while(temp)
	{
		if((double) rand()/RAND_MAX <= DECOMPOSITION_RATE) 
		{
			new_rayon = (temp->particule.rayon)*R_PARTICULE_FACTOR;
			nouvelle_energie = (temp->energie)*E_PARTICULE_FACTOR;

			if (new_rayon >= R_PARTICULE_MIN)
			{
				particule_new_generation(temp, new_rayon, nouvelle_energie);
				decompose = true;
				delete = temp;
				particule_suppression_particule(delete);
			}

			if(!temp->suivant)
				p_fin_liste = temp;
			temp = temp->suivant;
		}
		else
		{
			if(!temp->suivant)
				p_fin_liste = temp;
			temp = temp->suivant;
		}
	}
	return decompose;
}


void particule_suppression_particule(PARTICULE *particule)
{
	if (!particule)
		return;

	if(particule->precedent)
		(particule->precedent)->suivant = particule->suivant;
	else
	{
		particule_tete = particule->suivant;
		if (particule->suivant)
			(particule->suivant)->precedent = NULL;
	}

	if(particule->suivant)
		(particule->suivant)->precedent = particule->precedent;
	else
		p_fin_liste = particule;

	free(particule);
	particule = NULL;
	nb_particule--;
}


void particule_free_all(void)
{
	/* Retire un à un les elements en tete de la liste*/
	while (particule_tete)
		particule_suppression_particule(particule_tete);
	particule_tete = NULL;
	nb_particule = NOBODY;
}


void particule_draw_state(void)
{
	if(particule_tete)
	{
		PARTICULE * particule = particule_tete;
		while (particule)
		{
			utilitaire_draw_particule(particule->particule);
			particule = particule->suivant;
		}
	}
}


bool particule_detection_collision(void)
{
	PARTICULE * first = particule_tete;
	PARTICULE * second = particule_tete->suivant;
	double distance;
	int i = 0, j = 1;
	while (first->suivant)
	{
		while (second)
		{
			if (util_collision_cercle(first->particule, second->particule,
				&distance))
			{
				error_collision(PARTICULE_PARTICULE, nb_particule - i,
					nb_particule -j);
				return true;
			}
			second = second->suivant;
			j++;
		}
		first = first->suivant;
		second = first->suivant;
		i++;
		j=i+1;
	}
	return false;
}


C2D particule_current_c2d(bool first_time)
{
	if (first_time)
		collision_current = particule_tete;
	else
		collision_current = collision_current->suivant;
	return collision_current->particule;
}


void particule_enregistrement(FILE *save)
{
	fprintf(save, "%d\n", nb_particule);

	PARTICULE * particule = particule_tete;

	while (particule)
	{
		fprintf(save, "%g %g %g %g\n", particule->energie,
				particule->particule.rayon, particule->particule.centre.x,
				particule->particule.centre.y);

		particule = particule->suivant;
	}

	if(nb_particule)
		fprintf(save, "FIN_LISTE");
}


void particule_decontamination(PARTICULE *elue)
{
	if (elue)
		sd += elue->energie;
}


double particule_taux()
{
	return POURCENTAGE*sd/si;
}


void particule_taux_out(FILE *out, int turn)
{
	fprintf(out, "%d %lf \n", turn, POURCENTAGE*sd/si);
}
