//
//  graphic.h
//  Prog_II_Decontaminator
//
//  Created by Basile & Nicolas on 21/03/2018.
//  Copyright © 2018 Basile & Nicolas. All rights reserved.
//	Modified on 19/04/2018.

#ifndef GRAPHIC_H
#define GRAPHIC_H

#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <GL/glu.h>
#define NB_COLOR			3
#define SIDES				50
#define GRAPHIC_FILLED		1
#define GRAPHIC_EMPTY		0
#define CENTER				0

enum Color {NO_COLOR, ORANGE, GREY};


/**
 *	\brief	change la couleur du fond
 */
void graphic_set_background_color(void);

/**
 *	\brief	dessine un segment
 *	\param	x1	y1	x2	y2
 */
void graphic_draw_segment (double x1, double y1, double x2, double y2);

/**
 *	\brief	dessine un carré de coté DMAX plein ou vide
 *	\param	xc	yc	filled
 */
void graphic_draw_carre(double length, int filled);
/**
 *	\brief	dessine un cercle plein ou vide
 *	\param	xc	yc	r	filled	color
 */
void graphic_draw_circle (double xc, double yc, double r, int filled, int color);

/**
 *	\brief	définit la couleur
 *	\param	color
 */
void graphic_set_color3fv(float color[NB_COLOR]);

/**
 *	\brief	définit la largeur des traits
 *	\param	width
 */
void graphic_set_line_width(float width);



#endif
