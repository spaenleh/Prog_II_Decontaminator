#ifndef CONSTANTES_H
#define CONSTANTES_H

#include "tolerance.h"

#define DELTA_T				0.25
#define VTRAN_MAX			0.75
#define VROT_MAX			0.5
#define VTRAN_DEFAULT       0.000
#define VROT_DEFAULT        0.000
#define VTRAN_INCREM        0.250
#define VROT_INCREM         0.125
#define DELTA_VROT			0.125
#define DELTA_VTRAN			0.25
#define VITESSE_NULLE		0
#define DMAX 				20
#define TAILLE_CHAINE       20
#define R_ROBOT				0.5
#define R_PARTICULE_MAX		4
#define R_PARTICULE_MIN		0.3
#define R_PARTICULE_FACTOR	0.4142
#define E_PARTICULE_MAX 	1
#define E_PARTICULE_MIN		0.
#define E_PARTICULE_FACTOR	0.25
#define DECOMPOSITION_RATE	0.025
#define MAX_LINE 			120

#define L_LINE_9 			10
#define L_LINE_2 			2
#define DEBUT				0

#define WINDOWSIZE 			700
#define WINDOWPOSITION 		200
#define TEXTSIZE 			20
#define NOM_FICHIER 		2
#define MODE 				1
#define VALID_ARG			3
#define ASPECT_RATIO		1.0

enum State {INITIALISATION, SIMULATION};

#define NO_ONE				-1
#define BLOCAGE_MAX			55
#define P_TETE				1
#define NOBODY				0
#define POURCENTAGE			100

#define NB_PART_DECOMPOSE	4

enum Blocage {NO_BLOCAGE, BLOCAGE};




#endif
