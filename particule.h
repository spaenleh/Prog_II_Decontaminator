//
//  particule.h
//  Prog_II_Decontaminator
//
//  Created by Basile & Nicolas on 21/03/2018.
//  Copyright © 2018 Basile. All rights reserved.
//	Modified on 21/05/2018.

#ifndef PARTICULE_H
#define PARTICULE_H

#include <stdio.h>
#include "utilitaire.h"
#include "simulation.h"

typedef struct Particule PARTICULE;


/**
 *	\brief	retourne le taux de décontamination actuel
 */
double particule_taux(void);

/**
 *	\brief	retourne le C2D de la partciule pointée par part
 *	\param	part
 */
C2D particule_get_c2d_adr(PARTICULE *part);

/**
 *	\brief	retoune l'qdresse de la particule d'indice part
 *	\param	part
 */
PARTICULE * particule_get_adr(int part);

/**
 *	\brief	verifie si il y a une collision avec une particule
 *	\param	futur	vtran	vrot	indice
 */
PARTICULE * particule_adapt_collision(C2D futur, double *vtran, double *vrot, 
									  int indice);

/**
 *	\brief	trouve la particule située au S2D position
 *	\param	position
 */
PARTICULE * particule_position_location(S2D position);

/**
 *	\brief	renvoie le C2D de la particule d'indice part dans la liste
 *	\param	part
 */
C2D particule_get(int part);

/**
 *	\brief	trie la liste de particules
 */
void particule_tri_insertion(void);

/**
 *	\brief	cree les nouvelles particules suite à une decomposition
 *	\param	temp	new_rayon	new_energie
 */
void particule_new_generation(PARTICULE *temp, double new_rayon,
							  double new_energie);

/**
 *	\brief	renvoie l'adresse de la particule de rayon nv_rayon
 *	\param	nv_rayon
 */
PARTICULE * particule_rayon_location(double nv_rayon);

/**
 *	\brief	renvoie le nombre de partciules
 */
int particule_get_nb_particules(void);

/**
 *	\brief	trouve la particule la plus proche de robot
 *	\param	robot
 */
PARTICULE * particule_find_closest(C2D robot);

/**
 *	\brief	évalue la décomposition des particules
 */
bool particule_decomposition(void);

/**
 *	\brief	ajoute une particule en tête
 *	\param	i	e	r	x	y
 */
bool particule_add_particule(int i, double e, double r, double x, double y);

/**
 *	\brief	supprime une particule et libère la mémoire
 *	\param	particule
 */
void particule_suppression_particule(PARTICULE *particule);

/**
 *	\brief	libère la mémoire de toute les particules
 */
void particule_free_all(void);

/**
 *	\brief	dessine les particules
 */
void particule_draw_state(void);

/**
 *	\brief	détecte les collisions P-P
 */
bool particule_detection_collision(void);

/**
 *	\brief	initialise le nombre de particules
 *	\param	nb_objects
 */
void particule_set_nb_particule(int nb_objects);

/**
 *	\brief	fournit les infos nécessaires
 *	\param	first_time
 */
C2D particule_current_c2d(bool first_time);

/**
 *	\brief	fournit le S2D de la partciule d'indice indice
 *	\param	indice
 */
S2D particule_get_s2d(int indice);

/**
 *	\brief	enregistre les infos des partivules dans save
 *	\param	save
 */
void particule_enregistrement(FILE *save);

/**
 *	\brief	enregistre le taux de décontamination dans out
 *	\param	out 	turn
 */
void particule_taux_out(FILE *out, int turn);

/**
 *	\brief	incrémente l'énergie décontaminée lors de la supression d'une particule
 *	\param	out 	turn
 */
void particule_decontamination(PARTICULE *elue);

/**
 *	\brief	initialise le taux lors de la lecture
 */
void particule_init_taux(void);

#endif
