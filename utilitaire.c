//
//  utilitaire.c
//  Prog_II_Decontaminator
//
//  Created by Basile & Nicolas on 02/03/2018.
//  Copyright © 2018 Basile & Nicolas. All rights reserved.
//	Modified on 21/05/2018.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include "utilitaire.h"


// renvoie la distance entre les points a et b
double util_distance(S2D a, S2D b)
{
	return sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
}

// renvoie l'angle que fait le bipoint ab et l'axe X du monde.
// L'angle doit être en radians et compris dans l'intervalle ]-pi, +pi]
double util_angle(S2D a, S2D b)
{
	double dist_x = b.x - a.x;
	double dist_y = b.y - a.y;
	
	return atan2(dist_y, dist_x);
}

// modifie si nécessaire l'angle pointé par p_angle
// pour qu'il soit compris dans l'intervalle ]-pi, +pi]
void util_range_angle(double * p_angle)
{
 	if (*p_angle <= M_PI && *p_angle > -M_PI)
		return;
		
	double multiple = floor(*p_angle/(2*M_PI));
	*p_angle -= multiple*2*M_PI;
}

// renvoie VRAI si le point est en dehors du domaine [-max, max]
bool util_point_dehors(S2D a, double max)
{
	if (a.x > max || a.y > max || a.x < -max || a.y < -max)
		return true;
	else
		return false;
}

// renvoie VRAI si l'angle alpha est en dehors de l'intervalle [-pi, pi]
bool util_alpha_dehors(double alpha)
{
	if (alpha > M_PI || alpha < -M_PI)
		return true;
	else
		return false;
}

// renvoie VRAI si le point a est dans le cercle c
// plus précisément: si la distance de a au centre de c  < rayon - EPSIL_ZERO
bool util_point_dans_cercle(S2D a, C2D c)
{
	if (util_distance(a, c.centre) < c.rayon - EPSIL_ZERO)
		return true;
	else
		return false;
}

// renvoie VRAI en cas de collision des cercles a et b selon l'Equ. 4
// le paramètre de sortie p_dist est la distance entre les centres de a et b
bool util_collision_cercle(C2D a, C2D b, double * p_dist)
{
	*p_dist = util_distance(a.centre, b.centre);
	if (*p_dist < (a.rayon + b.rayon) - EPSIL_ZERO)
		return true;
	else
		return false;
}

// renvoie la position obtenue après déplacement du point p d'une distance dist
// dans la direction définie par l'angle alpha
S2D util_deplacement(S2D p, double alpha, double dist)
{
	p.x += cos(alpha)*dist;
	p.y += sin(alpha)*dist;
	return p;
}

// renvoie VRAI si la distance de a à b > EPSIL_ZERO et renvoie FAUX sinon.
// DE PLUS, dans le cas VRAI on utilise p_ecart_angle (qui doit être défini)
// pour récupérer l'écart angulaire entre le bipoint ab et un vecteur d'angle
// alpha.
// La valeur de l'écart angulaire doit être dans l'intervalle [-pi, pi].
bool util_ecart_angle(S2D a, double alpha, S2D b, double *p_ecart_angle)
{
	if (util_distance(a, b) > EPSIL_ZERO)
	{
		*p_ecart_angle = util_angle(a, b) - alpha;
		util_range_angle(p_ecart_angle);
		return true;
	} else
		return false;
}

// renvoie VRAI si un vecteur d'angle alpha est aligné avec le vecteur ab. POur
// déterminer cela on obtient l'écart angulaire avec la fonction
// util_ecart_angulaire
// et on revoie VRAI si cette fonction renvoie VRAI et si la valeur absolue de
// cet écart angulaire < EPSIL_ALIGNEMENT. Renvoie FAUX pour tous les autres cas.
bool util_alignement(S2D a, double alpha, S2D b)
{
	double ecart_angle;
	double * p_ecart_angle = &ecart_angle;
	
	if (util_ecart_angle(a, alpha, b, p_ecart_angle)
		&& fabs(*p_ecart_angle) < EPSIL_ALIGNEMENT)
		return true;
	else
		return false;
}

// renvoie VRAI si on peut calculer la nouvelle longueur du coté a lorsqu'on
// change la longueur du coté b, la longueur du coté c restant constante.
// Les longueurs des cotés a,b,c sont notées la, lb, lc. La nouvelle longueur
// du coté b est lb_new. Le paramètre de sortie p_la_new doit être défini.
// Renvoie VRAI si:
// les 3 longueurs la et lc > EPSIL_ZERO et lb >= 0, lb_new se trouve dans
// l'intervalle
// autorisé [lb, lc]. Le calcul de la_new est donné par l'Equ.5 qui résoud le cas
// particulier de la Fig 5c avec:
//     la     = delta_d, lb = D, lc = L, lb_new = r1+r2
//     la_new = delta_d'
//
bool util_inner_triangle(double la, double lb, double lc, double lb_new,
						 double * p_la_new)
{
	if (la > EPSIL_ZERO && lc > EPSIL_ZERO && lb >= 0 && lb_new >= lb &&
		lb_new <= lc)
	{
		double b = (la*la + lc*lc - lb*lb)/la;
		double discrim = b*b - 4*(lc*lc - lb_new*lb_new);
		if (discrim >= 0)
			if(p_la_new)
			{
				*p_la_new = (b - sqrt(discrim))*0.5;
				return true;
			}
	}
	return false;
}



void utilitaire_draw_robot(C2D robot, double angle, bool selected)
{
	//graphic_set_background_color();
	//glClear(GL_COLOR_BUFFER_BIT);
	double rayon_interieur = 0.1;
	double x_robot = robot.centre.x;
	double y_robot = robot.centre.y;
	double x_end_segment = x_robot + robot.rayon*cos(angle);
	double y_end_segment = y_robot + robot.rayon*sin(angle);
	graphic_set_line_width(2.);
	if(selected)
		graphic_draw_circle(x_robot, y_robot, robot.rayon, GRAPHIC_EMPTY, ORANGE);
	else
		graphic_draw_circle(x_robot, y_robot, robot.rayon, GRAPHIC_EMPTY, NO_COLOR);
	
	graphic_draw_segment(x_robot, y_robot, x_end_segment, y_end_segment);
	graphic_draw_circle(x_robot, y_robot, rayon_interieur, GRAPHIC_FILLED,
						ORANGE);
}

void utilitaire_draw_particule(C2D particule)
{
	graphic_set_line_width(2.);
	graphic_draw_circle(particule.centre.x, particule.centre.y, particule.rayon,
						GRAPHIC_FILLED, GREY);
}

void util_dessiner_cadre(int length)
{
	graphic_draw_carre(length, GRAPHIC_EMPTY);
}
