//
//  graphic.c
//  Prog_II_Decontaminator
//
//  Created by Basile & Nicolas on 21/03/2018.
//  Copyright © 2018 Basile & Nicolas. All rights reserved.
//	Modified on 19/04/2018.

#include "graphic.h"

static float  black [] = { 0., 0., 0. },
grey  [] = { 0.6, 0.6, 0.6},
orange [] = { 1., 0.4, 0.13},
white [] = { 1., 1., 1. },
red   [] = { 1., 0., 0. },
blue  [] = { 0.173, 0.560, 1. };

void graphic_set_background_color(void)
{
	glClearColor(1.,1.,1.,1.);
}


void graphic_draw_segment (double x1, double y1, double x2, double y2)
{ 
	glBegin (GL_LINES);
	
	glVertex2f (x1, y1);
	glVertex2f (x2, y2);
	
	glEnd ();
}


void graphic_draw_carre(double length, int filled)
{
	if (filled == GRAPHIC_FILLED)
	{
		glBegin (GL_POLYGON);
		graphic_set_color3fv(grey);
	}
	else
	{
		glBegin (GL_LINE_LOOP);
		graphic_set_color3fv(grey);
	}
	
	glVertex2f (length, length);
	glVertex2f (-length, length);
	glVertex2f (-length, -length);
	glVertex2f (length, -length);
	
	glEnd ();
}


void graphic_draw_circle (double xc, double yc, double r, int filled, int color)
{
	int i;
	
	if (filled == GRAPHIC_FILLED)
	{
		glBegin (GL_POLYGON);
		graphic_set_color3fv(blue);
	}
	else
	{
		glBegin (GL_LINE_LOOP);
		graphic_set_color3fv(black);
	}
	if(color == ORANGE)
		graphic_set_color3fv(orange);
	
	for (i=0; i < SIDES; i++)
	{
		double alpha = i * 2. * M_PI / SIDES;
		glVertex2f (xc + r*cos(alpha), yc + r*sin(alpha));
	}
	glEnd ();
}


void graphic_set_color3fv(float color[NB_COLOR])
{
	glColor3fv(color);
}


void graphic_set_line_width(float width)
{
	glLineWidth(width);
}

