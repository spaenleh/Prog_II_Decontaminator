//
//  simulation.c
//  Prog_II_Decontaminator
//
//  Created by Basile & Nicolas on 21/03/2018.
//  Copyright © 2018 Basile & Nicolas. All rights reserved.
//	Modified on 21/05/2018.

#include "simulation.h"

enum Automate {NO_OBJECT, LECTURE_OK, INCREMENTATION, NB_VAR_ROBOT, 
			   NB_VAR_PARTICULE};
enum Mode {FIN_PARTICULE, FIN_ROBOT, ERROR, DRAW};
enum Etat_lecture {NB_ROBOT, ETAT_ROBOT, FIN_LISTE_ROBOTS, NB_PARTICULE,
				   ETAT_PARTICULE, FIN_LISTE_PARTICULES, FIN};

// variables globales au module
static FILE * fichier_in = NULL;
static FILE * fichier_out = NULL;
static FILE * sortie_dat = NULL;
static unsigned int line_number;
static int mode = DRAW;
static int etat = NB_ROBOT; // état initial
static int i, nb_robots, nb_particules;
static int turn = 0;
static char * deb = NULL;
static bool afficher_draw;
static PARTICULE **goal_tab;
static int last_update = 0;


int simulation_get_turn(void)
{
	return turn;
}


void simulation_creat_goal_tab(void)
{
	if (!(goal_tab = (PARTICULE**) calloc(nb_robots, sizeof(PARTICULE*))))
	{
		printf("Allocation dynamique échouée\n");
	}
}


void simulation_no_more_particules(bool attributed[nb_robots])
{
	int i;
	C2D robot;
	for (i = 0; i < nb_robots; i++)
	{
		robot = robot_get_c2d(i);
		
		if (!attributed[i])
		{
			goal_tab[i] = particule_find_closest(robot);
			attributed[i] = true;
		}
	}
}


int simulation_random_particule(void)
{
	return (rand()/(double) RAND_MAX * (nb_particules - 1));
}


void simulation_goal(bool blocage)
{
	int part, indice, i, j, count = nb_robots;
	bool first, attributed[nb_robots];
	double dist, a_goal, time_catch_min, time_catch;

	for (i = 0; i < nb_robots; i++)
	 	attributed[i] = robot_manual_control(i);

	if(blocage)
	{
		for (i = 0; i < nb_robots; i++)
		{
			goal_tab[i] = particule_get_adr(simulation_random_particule());
			attributed[i] = true;
		}
	}
	else
	{
		for (part = 0; part < nb_particules && count > 0; part++, count--)
		{
			C2D robot, particule = particule_get(part);
			first = true;
			
			for (j = 0; j < nb_robots; j++)
			{
				if (!attributed[j])
				{
					robot = robot_get_c2d(j);
					dist = util_distance(robot.centre, particule.centre);
					a_goal = util_angle(robot.centre, particule.centre)
							 - robot_get_angle(j);
					time_catch = (fabs(a_goal) / VROT_MAX) + (dist / VTRAN_MAX);

					if(first)
					{
						time_catch_min = time_catch;
						indice = j;
						first = false;
					}
					else
					{
						if (time_catch < time_catch_min)
						{
							time_catch_min = time_catch;
							indice = j;
						}
					}
				}
			}
			goal_tab[indice] = particule_get_adr(part);
			attributed[indice] = true;
		}
	}
	simulation_no_more_particules(attributed);
	last_update = turn;
}


bool simulation_step(double vtran_man, double vrot_man)
{
	if (!nb_particules)
		return false;
	turn ++;
	if(particule_decomposition())
	{
		nb_particules = particule_get_nb_particules();
		simulation_goal(NO_BLOCAGE);
	}
	if(turn - last_update > BLOCAGE_MAX)
		simulation_goal(BLOCAGE);

	int indice, count = nb_robots;
	double vtran, vrot;
	C2D futur, particule;
	for (indice = 0; indice < nb_robots; indice++)
	{
		if (robot_manual_control(indice))
		{
			vtran = vtran_man;
			vrot = vrot_man;
		}
		else
		{
			if (goal_tab[indice])
				particule = particule_get_c2d_adr(goal_tab[indice]);
			robot_evaluate_move(indice, particule, &vtran, &vrot);
		}
		robot_adapt_collision(indice, &vtran, &vrot);
		futur = robot_get_c2d(indice);
		double futur_angle = robot_get_angle(indice) + vrot*DELTA_T;
		futur.centre = util_deplacement(futur.centre, futur_angle, vtran*DELTA_T);
		futur.rayon = R_ROBOT;
		PARTICULE * temp = particule_adapt_collision(futur, &vtran, &vrot, indice);
		if (temp && !robot_manual_control(indice))
		{
			goal_tab[indice] = temp;
			count--;
			continue;
		}
		robot_mouvement(indice, &vtran, &vrot);
		if(robot_suppression_part(indice, vtran, particule))
		{
			nb_particules = particule_get_nb_particules();
			simulation_goal(NO_BLOCAGE);
		}
		if (!nb_particules)
		{
			if(sortie_dat)
				particule_taux_out(sortie_dat, turn);
			return false;
		}
	}
	if(sortie_dat)
		particule_taux_out(sortie_dat, turn);
	return true;
}

//******************************************************************//
//																	//
//					AUTOMATE DE LECTURE								//
//																	//
//******************************************************************//

void simulation_set_mode(char * mode_entree)
{
	if (mode_entree)
	{
		if (!strcmp(mode_entree, "Error\0"))
		{
			mode = ERROR;
		}
		else if (!strcmp(mode_entree, "Draw\0"))
		{
			mode = DRAW;
		}
		else
		{
			printf("Invalid mode entered, please enter 'Draw' or 'Error'\n");
			simulation_close_exit();
		}
	}
}


// traite le fichier ligne par ligne.
// * nom_fichier donée par main.cpp
bool simulation_lecture( const char * nom_fichier)
{
	char tab[MAX_LINE];
	
	// initialisation pour une nouvelle ouverture
	etat = NB_ROBOT;
	deb = NULL;
	afficher_draw = true;
	turn = 0;
	last_update = 0;
	particule_init_taux();
	
	if((fichier_in = fopen (nom_fichier, "r")) != NULL)
	{
		//ligne 0 car on incremente au debut dans le while
		line_number = 0; 
		while(fgets(tab, MAX_LINE, fichier_in) && afficher_draw)
		{
			line_number++;
			
			if(simulation_useless_line(tab))
			{
				continue;  // lignes à ignorer, on passe à la suivante
			}
			simulation_decodage_ligne(tab);
		}
		if(!afficher_draw)
			return false;
	}
	else
	{
		error_file_missing(nom_fichier);
		simulation_error_detected();
		return false;
	}
	
	simulation_collision_particule_robot(INITIALISATION);
	
	if (mode == ERROR)
	{
		error_no_error_in_this_file();
		simulation_free_all();
		simulation_close_exit();
	}
	fclose(fichier_in);
	fichier_in = NULL;

	particule_tri_insertion();
	
	simulation_creat_goal_tab();
	return true;
}


void simulation_decodage_ligne(char * tab)
{	
	switch(etat)
	{
		case NB_ROBOT:
			simulation_decodage_nb_robot(tab);
			break;

		case ETAT_ROBOT:
			simulation_decodage_robot(tab);
			break;

		case FIN_LISTE_ROBOTS:
			simulation_decodage_fin_liste(tab, FIN_ROBOT);
			break;

		case NB_PARTICULE:
			simulation_decodage_nb_particules(tab);
			break;
			
		case ETAT_PARTICULE:
			simulation_decodage_particule(tab);
			break;

		case FIN_LISTE_PARTICULES:
			simulation_decodage_fin_liste(tab, FIN_PARTICULE);
			break;

		default:
			// sert à prendre etat = FIN pour ne pas faire un case FIN: vide
			// case FIN: attends la fin du fichier
			break;
	}
}


void simulation_decodage_nb_robot(char * tab)
{
	//nb robot incorrect
	if(sscanf(tab,"%d", &nb_robots) != LECTURE_OK)
	{
		error_invalid_nb_robots();
		simulation_error_detected();
		return;
	}
	//nb robot est négatif
	if(nb_robots < 0)
	{
		error_invalid_nb_robots();
		simulation_error_detected();
	}

	/*ne pas appeller l'initialisation et donc laisser le pointeur de robot à 
	NULL comme ça on le teste et si il est null on fait rien.*/
	if(nb_robots == NO_OBJECT)
		etat=NB_PARTICULE;
	else
	{
		etat=ETAT_ROBOT;
		i=0;
		if(!robot_init_structure(nb_robots))
		{
			simulation_error_detected();
		}
	}
	//si on trouve un caractère inutile après la lecture de nb_robots
	strtod(tab, &deb);
	if (!simulation_useless_line(deb))
	{
		error_useless_char(line_number);
		simulation_error_detected();
		return;
	}
}


void simulation_decodage_robot(char * tab)
{
	double x, y, a;
	deb = tab;

	while (etat == ETAT_ROBOT && !simulation_useless_line(tab))
	{
		if(sscanf(tab," %lf %lf %lf", &x, &y, &a) != NB_VAR_ROBOT)
		{
			if (simulation_fin_liste(tab))
			{
				error_fin_liste_robots(line_number);
				simulation_error_detected();
			}
			else if (!simulation_useless_line(tab))
			{
				error_useless_char(line_number);
				simulation_error_detected();
				return;
			}
		}
		else
		{
			int k;
			if(!robot_add_robot(i, x, y, a))
			{
				simulation_error_detected();
				continue;
			}
			else
			{
				for (k=0; k<NB_VAR_ROBOT; k++)
				{
					strtod(tab, &deb);
					tab = deb;
				}
				i++;
			}
		}

		if(i == nb_robots)
		{
			etat=FIN_LISTE_ROBOTS;

			if(robot_detection_collision())
				simulation_error_detected();
		}
	}
}


void simulation_decodage_fin_liste(char * tab, bool robot)
{
	if(simulation_fin_liste(tab))
	{
		if(robot)
			etat = NB_PARTICULE;
		else
			etat = FIN;
	}
	else
	{
		if(robot)
			error_missing_fin_liste_robots(line_number);
		else
			error_missing_fin_liste_particules(line_number);
		simulation_error_detected();
	}
}


void simulation_decodage_nb_particules(char * tab)
{
	if(sscanf(tab,"%d", &nb_particules) != LECTURE_OK)
	{
		error_invalid_nb_particules();
		simulation_error_detected();
		return;
	}
	
	//nb_particule négative
	if(nb_particules < 0)
	{
		error_invalid_nb_particules();
		simulation_error_detected();
	}
	/*si pas de particules, ne pas appeller l'initialisation 
	c.à.d laisser le pointeur de particules à NULL*/
	if(nb_particules == NO_OBJECT)
		etat=FIN;
	else
	{
		particule_set_nb_particule(nb_particules);
		etat=ETAT_PARTICULE;
		i=0;
	}
	//si on trouve un caractère inutile après la lecture de nb_particules
	strtod(tab, &deb);
	if (!simulation_useless_line(deb))
	{
		error_useless_char(line_number);
		simulation_error_detected();
	}
}


void simulation_decodage_particule(char * tab)
{
	double x, y, e, r;
	deb = tab;
	while (etat == ETAT_PARTICULE && !simulation_useless_line(tab))
	{
		if(sscanf(tab,"%lf %lf %lf %lf", &e, &r, &x, &y) != NB_VAR_PARTICULE)
		{
			if (simulation_fin_liste(tab))
			{
				error_fin_liste_particules(line_number);
				simulation_error_detected();
			}
			if (simulation_useless_line(tab))
				return;
		}
		else
		{
			int k;
			if(!particule_add_particule(i, e, r, x, y))
			{
				simulation_error_detected();
				continue;
			}
			for (k=0; k < NB_VAR_PARTICULE; k++)
			{
				strtod(tab, &deb);
				tab = deb;
			}
			i++;
		}

		if(i == nb_particules)
		{
			etat=FIN_LISTE_PARTICULES;

			if (particule_detection_collision())
				simulation_error_detected();
		}
	}
}


bool simulation_useless_line(char * tab)
{
	char test[L_LINE_2];
	if (tab[DEBUT] == '\n')
		return true;
	sscanf(tab, " %1s", test);
	return (test[DEBUT]=='#'||test[DEBUT]=='\r'||test[DEBUT]=='\0') ? true : false;
}


bool simulation_fin_liste(char * tab)
{
	char fin[L_LINE_9] = "FIN_LISTE";
	char fin_list[L_LINE_9];
	
	sscanf(tab, " %9s", fin_list);
	
	if(!strcmp(fin_list, fin))
		return true; //chaines fin_liste détectée
	else
		return false; //chaine fin_liste non détectée
}


void simulation_free_all(void)
{
	if (goal_tab)
	{
		free(goal_tab);
		goal_tab=NULL;
	}
	robot_free_all();
	particule_free_all();
}


void simulation_close_exit()
{
	if(fichier_in)
		fclose(fichier_in);
	if(fichier_out)
		fclose(fichier_out);
	if(sortie_dat)
		fclose(sortie_dat);
	exit(EXIT_FAILURE);
}


void simulation_draw_state(void)
{
	util_dessiner_cadre(DMAX);
	if(afficher_draw)
	{
		robot_draw_state();
		particule_draw_state();
	}
}


void simulation_error_detected(void)
{
	if (mode == ERROR)
	{
		simulation_free_all();
		simulation_close_exit();
	}
	if (mode == DRAW)
	{
		afficher_draw = false;
		etat = FIN;
		simulation_free_all();
		robot_init_structure(false);
		particule_set_nb_particule(false);
	}
}


void simulation_collision_particule_robot(int state)
{
	//collision robot-particule
	int j, k;

	for (j=0; j < nb_particules; j++)
	{
		C2D particule;
		if (j==0)
			particule = particule_current_c2d(true);
		else
			particule = particule_current_c2d(false);
		
		for (k=0; k < nb_robots; k++)
		{
			C2D robot;
			double distance;
			robot = robot_current_c2d(k);
			
			if (util_collision_cercle(particule, robot, &distance))
			{
				error_collision(ROBOT_PARTICULE, k+1, nb_particules - j);
				simulation_error_detected();
				return;
			}
		}
	}
}


void simulation_record(const char* out, bool checkbox)
{
	if(checkbox)
		sortie_dat = fopen(out, "w");

	if(!checkbox)
	{
		if(sortie_dat)
		{
			fclose(sortie_dat);
			sortie_dat = NULL;
		}
	}
}


void simulation_enregistrement(const char * nom_fichier)
{
	fichier_out = fopen(nom_fichier, "w+");
	
	fputs("#fichier copié\n", fichier_out);
	
	robot_enregistrement(fichier_out);
	
	particule_enregistrement(fichier_out);
	
	fputs("\n", fichier_out);

	fclose(fichier_out);

	fichier_out = NULL;
}

//Fin du fichier simulation.c
